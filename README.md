# K8s Learning


## 1.0 Introduction to Kubernetes via Minikube:

Objective: Explain the [video](https://www.youtube.com/watch?v=d6WC5n9G_sM&ab_channel=freeCodeCamp.org) tutorial in a read format and have some important commands and links saved for future references.   
Credits: [Bogdan Stashchuk](https://www.youtube.com/c/CodingTutorials) || [freecodingcamp.org](https://www.youtube.com/c/Freecodecamp)

What is Kubernetes:
1. Container orchestration service.  

Use cases of K8s:
1. Automatic deployment and replacement of containerised applications.  
2. Automatic distribution of load across multiple servers.  
3. Auto scaling of deployed applications according to obserbved use.
4. Dashboard operations for health checks.  

What kind of container runtimes are supported?
1. Docker(default)
2. containerd
3. CRI-O

As Docker has it's smallest unit in Container, similarly K8s has it's smallest unit defined in Pods.
 
![](./images/POD_SK.png)


There is always a "Master Node" and it manages other nodes called as "Worker Node". All our deployments will be on worker nodes, only system deployments are there on master pod.

![](./images/K8s_Services.png)

[Official Documentation](https://kubernetes.io/docs/concepts/overview/components/) for reference and detailed study of the diifferent components for K8s. 

## 1.1 Required Software:

1. **kubectl:** Command line tool for sending request to API-server in master node. 
```shell
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```  
Check Installations:   
```shell
kubectl version --client
```  
2. **Minikube:** Service provider for K8s. Other options: kind(local), k3s(local), cloud providers.   
Works both as a master and worker nodes and ideal for playground purposes.  
```shell
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
```  
Check Installations:   
```shell
minikube version
```  
3. **VirtualBox:** A virtual machine or container manager is required, remember to select a VM ware which is optimised for your operating system.
Default is Docker but it is not ideal as there will be Docker inside Docker situation which I avoid.
```shell
sudo apt-get install virtualbox virtualbox-qt virtualbox-dkms 
```  
Check Installations:   
```shell
vboxmanage --version
```  
Note: though **kubetctl** is included in **minikube**, a seperate installation is preferred. Commands will differ `minikube kubectl` vs `kubectl`. Also usually sole **kubectl** installation will also make it possible to use cloud based service providers.

## 1.2 Create Minkube Cluster

Enter `minikube config set driver virtualbox` to change the default driver from Docker to VirtualBox.  
After that:  

![](./images/minikube_start.png)

Since, Node == Server so let's connect to the master node. Note: `minikube ssh` also works and is the only way to enter if driver is Docker.  

![](./images/minikube_node.png)  
Pwd: `tcuser`  

## 1.3 Minikube: Creating Pods

To create a single pod:  

![](./images/minikube_pods.png)  

Details about the pod created:

![](./images/minikube_describe_1.png)  
![](./images/minikube_describe_2.png)  


Commands used:  

```shell
minikube start
minikube status
minikube ip
ssh docker@192.168.59.102
kubectl cluster-info
kubectl get nodes
kubectl get namespace
kubectl get pods
kubectl get pods --namespace=kube-system
kubectl run nginx --image=nginx
kubectl describe pod nginx
```  

## 1.4 Minikube: Exploring Pods

Now that we have created a pod, let's connect to the cluster and get inside the container and connect to the webserver running inside it with a `curl` command.  

![](./images/exploring_K8s_pod.png)  

Commands used:  

```shell
ssh docker@192.168.59.102
docker ps |grep nginx
hostname
hostname -i
curl 172.17.0.3
```  

The ip address above seen in the container can also be seen as the same for the pod, also remember that if multiple containers are inside one pod then they share the ip address. We further try to connect to the ip address from our local machine but it fails since we are external to the cluster(hosted on a VM).  

![](./images/minikube_delete_pod.png)  

Commands used:  

```shell
kubectl get pods -o wide
curl 172.17.0.3
kubectl delete pod nginx
kubectl get pods
```  

## 1.5 Minikube: Creating Deployment  

To scale creation of pods and modifying them is through the use of deployment. All the pods inside one deployment will always be the same but the load can now be distributed efficiently.  

![](./images/minikube_create_deployment.png)  

Now we take any one of the pods from the deployment and analyse it. importnt to note is the field `Controlled By` and `pod-template-hash`

![](./images/minikube_dep_pod_1.png)    
![](./images/minikube_dep_pod_2.png)   
![](./images/minikube_ip_pods.png)  

Commands used:  

```shell
kubectl create deployment nginx-deployment --image=nginx
kubectl get deployments
kubectl get pods
kubectl describe deployment
kubectl scale deployment nginx-deployment --replicas=5
kubectl describe pod nginx-deployment-85c6d5f6dd-hxwnc
kubectl get pods -o wide
```  

We can as previously seen also connect to the cluster and use the ip of the pods mentioned to get the response from the nginx-server inside the container. Drawback is we are still not able to connect from outside the cluster and say from our local machine.

## 1.6 Introducing K8s Services

1. If the pods are dependent on each other and need ip to communicate, then we cannot have ip changing with time based on health of the pod. 
2. We want to expose a common ip address to outside of the cluster and load balance the requests comming in.

By default nginx-server is running on port:80 inside the container and we will like to expose port:8080 from our deployment. In the following example of creating the service we use the type "ClusterIP" which means that the mentioned IP can be be used from only inside the cluster but instead of using individual unique Pod IP we use the Service IP.  

![](./images/minikube_cluster_ip.png)
![](./images/minikube_describe_service.png)  

Commands used:  

```shell
kubectl expose deployment nginx-deployment --port=8080 --target-port=80
kubectl get services
curl 10.103.35.87:8080
ssh docker@192.168.59.102
kubectl describe service nginx-deployment
kubectl delete deployment nginx-deployment
kubectl delete service nginx-deployment
```  

## 2.0 Creating and Deploying a dummy Application

----

#### Roadmap
Will add more in-depth examples.

#### Contributing
Open to all.

#### Authors and acknowledgment
[Bogdan Stashchuk](https://www.youtube.com/c/CodingTutorials)

#### License
Not required.

#### Project status
If you want to extend it, please send a merge request.
